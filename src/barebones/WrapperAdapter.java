package barebones;

import com.ib.client.CommissionReport;
import com.ib.client.Contract;
import com.ib.client.ContractDetails;
import com.ib.client.EClientSocket;
import com.ib.client.EWrapper;
import com.ib.client.Execution;
import com.ib.client.Order;
import com.ib.client.OrderState;
import com.ib.client.UnderComp;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedWriter;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

public class WrapperAdapter implements EWrapper {

    EClientSocket client = new EClientSocket(this);
    public static List<Double> strikes = new ArrayList<>();
    public static List<String> strikeid = new ArrayList<>();
    public static int strikecount;
//    private static HashMap<Integer, List<String>> histdate = new HashMap<>();
//    private static HashMap<Integer, List<Double>> histopen = new HashMap<>();
//    private static HashMap<Integer, List<Double>> histhigh = new HashMap<>();
//    private static HashMap<Integer, List<Double>> histlow = new HashMap<>();
//    private static HashMap<Integer, List<Double>> histclose = new HashMap<>();
//    private static HashMap<Integer, List<String>> timestamp = new HashMap<>();
    private static List<String> dateTempArray = new ArrayList<>();
    private static List<Double> openTempArray = new ArrayList<>();
    private static List<Double> highTempArray = new ArrayList<>();
    private static List<Double> lowTempArray = new ArrayList<>();
    private static List<Double> closeTempArray = new ArrayList<>();
    private static List<String> histdate = new ArrayList<>();
    private static List<Double> histopen = new ArrayList<>();
    private static List<Double> histhigh = new ArrayList<>();
    private static List<Double> histlow = new ArrayList<>();
    private static List<Double> histclose = new ArrayList<>();
    private static List<String> timestamp = new ArrayList<>();
//    private static ArrayList<Double> complow = new ArrayList<>();
//    private static ArrayList<Double> comphigh = new ArrayList<>();
    public static double complow = 0.0;
    public static double comphigh = 0.0;
    private static int nameOftrade = 0;
    private static int nameofstrike = 0;

    @Override
    public void tickPrice(int tickerId, int field, double price, int canAutoExecute) {
//        System.out.println("the price is" + price);
    }

    @Override
    public void tickSize(int tickerId, int field, int size) {
    }

    @Override
    public void tickOptionComputation(int tickerId, int field, double impliedVol, double delta, double optPrice, double pvDividend, double gamma, double vega, double theta, double undPrice) {
    }

    @Override
    public void tickGeneric(int tickerId, int tickType, double value) {
    }

    @Override
    public void tickString(int tickerId, int tickType, String value) {
    }

    @Override
    public void tickEFP(int tickerId, int tickType, double basisPoints, String formattedBasisPoints, double impliedFuture, int holdDays, String futureExpiry, double dividendImpact, double dividendsToExpiry) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void orderStatus(int orderId, String status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId, String whyHeld) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openOrder(int orderId, Contract contract, Order order, OrderState orderState) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openOrderEnd() {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateAccountValue(String key, String value, String currency, String accountName) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updatePortfolio(Contract contract, int position, double marketPrice, double marketValue, double averageCost, double unrealizedPNL, double realizedPNL, String accountName) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateAccountTime(String timeStamp) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void accountDownloadEnd(String accountName) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void nextValidId(int orderId) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void contractDetails(int reqId, ContractDetails contractDetails) {
        //   System.out.println("Strike values "+contractDetails.m_summary.m_strike);

        strikes.add(contractDetails.m_summary.m_strike);
        strikeid.add(contractDetails.m_industry);

    }

    @Override
    public void bondContractDetails(int reqId, ContractDetails contractDetails) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void contractDetailsEnd(int reqId) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void execDetails(int reqId, Contract contract, Execution execution) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void execDetailsEnd(int reqId) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateMktDepth(int tickerId, int position, int operation, int side, double price, int size) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateMktDepthL2(int tickerId, int position, String marketMaker, int operation, int side, double price, int size) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateNewsBulletin(int msgId, int msgType, String message, String origExchange) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void managedAccounts(String accountsList) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void receiveFA(int faDataType, String xml) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void historicalData(int reqId, String date, double open, double high, double low, double close, int volume, int count, double WAP, boolean hasGaps) {
          System.out.println("tickerid:"+reqId+"datetimestamp"+date+"trade opening"+open+"trade high"+high+"trade LOW"+low+"trade Close"+close);

//
//        if (!date.contains("finished")) {
////            System.out.println("tickerid:" + reqId + " High Value " + highTempArray.size());
//            dateTempArray.add(date);
//            openTempArray.add(open);
//            highTempArray.add(high);
//            lowTempArray.add(low);
//            closeTempArray.add(close);
////            histdate.add(date);
////            histopen.add(open);
////            histhigh.add(high);
////            histlow.add(low);
////            histclose.add(close);
//
//        } else {
//            //    System.out.println("tickerid:" + reqId + " High Values " + openTempArray.size());
//            timestamp.add(date);
////            histdate.put(reqId, dateTempArray);
////            histopen.put(reqId, openTempArray);
////            histhigh.put(reqId, highTempArray);
////            histlow.put(reqId, lowTempArray);
////            histclose.put(reqId, closeTempArray);
//            histdate = dateTempArray;
//            histopen = openTempArray;
//            histhigh = highTempArray;
//            histlow = lowTempArray;
//            histclose = closeTempArray;
//
//            dateTempArray = new ArrayList<>();
//            openTempArray = new ArrayList<>();
//            highTempArray = new ArrayList<>();
//            lowTempArray = new ArrayList<>();
//            closeTempArray = new ArrayList<>();
//
//           
//        }


    }

    @Override
    public void scannerParameters(String xml) {
    }

    @Override
    public synchronized void scannerData(int reqId, int rank, ContractDetails contractDetails, String distance, String benchmark, String projection, String legsStr) {
    }

    public synchronized void getData(int reqId, int rank, ContractDetails contractDetails, String distance, String benchmark, String projection, String legsStr) {
    }

    @Override
    public synchronized void scannerDataEnd(int reqId) {
    }

    @Override
    public void realtimeBar(int reqId, long time, double open, double high, double low, double close, long volume, double wap, int count) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void currentTime(long time) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fundamentalData(int reqId, String data) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deltaNeutralValidation(int reqId, UnderComp underComp) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void tickSnapshotEnd(int reqId) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void marketDataType(int reqId, int marketDataType) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void commissionReport(CommissionReport commissionReport) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void position(String account, Contract contract, int pos) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void positionEnd() {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void accountSummary(int reqId, String account, String tag, String value, String currency) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void accountSummaryEnd(int reqId) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void error(Exception e) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void error(String str) {
        System.err.println(str);
    }

    @Override
    public void error(int id, int errorCode, String errorMsg) {
         System.err.println(errorMsg);
    }

    @Override
    public void connectionClosed() {
        //To change body of generated methods, choose Tools | Templates.
    }

    public static void max_min_Values() {

        comphigh = (Collections.max(histhigh));
        complow = (Collections.min(histlow));
             System.out.println(comphigh);
              System.out.println(complow);

    }
}
