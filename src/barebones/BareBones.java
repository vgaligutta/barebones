/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package barebones;

import com.ib.client.Contract;

/**
 *
 * @author Administrator
 */
public class BareBones {
    static WrapperAdapter wa;
    static Contract con;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         try {
            wa = new WrapperAdapter();
            wa.client.eConnect("127.0.0.1", 7496, 1);
            System.out.println("client status =" + wa.client.isConnected());
            con = new Contract();
            con.m_currency = "USD";
            con.m_right = "C";
            con.m_multiplier = "100";
            con.m_exchange = "SMART";
            con.m_symbol = "YHOO";
            con.m_expiry = "20131018";
            con.m_strike = 33;
            con.m_secType = "OPT";
            wa.client.reqHistoricalData(1, con,"20130927 16:00:00","1 D", "1 min","TRADES",1,1)  ;
         }
         catch (Exception e) {

            System.err.println("exception at reqcontract " + e);
        }
         
    }
}
